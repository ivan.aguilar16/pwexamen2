from django.urls import path,include
from act2 import views

urlpatterns = [
    path('listsoft/',views.List.as_view(),name = "listsoft"),
    path('createsoft/',views.CreateSoft.as_view(),name = "createsoft"),
    path('createdepa/',views.CreateDepa.as_view(),name="createdepa"),
    path('updatesoft/<int:pk>',views.UpdateSoft.as_view(),name="updatesoft"),
    path('updatedepa/<int:pk>',views.UpdateDepa.as_view(),name="updatedepa"),
    path('detailsoft/<int:pk>/', views.DetailSoft.as_view(), name="detailsoft"),
]
