from django import forms

from .models import Depa, Soft

class DepaForm(forms.ModelForm):
    class Meta:
        model = Depa
        fields = [
        "name_depa",
        "slug",
        ]

class SoftForm(forms.ModelForm):
    class Meta:
        model = Soft
        fields = [
        "name_soft",
        "desc",
        "depa",
        "slug",
        ]
