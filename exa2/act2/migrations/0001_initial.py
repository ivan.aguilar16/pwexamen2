# Generated by Django 2.2.6 on 2019-10-19 04:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Depa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_depa', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Soft',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_soft', models.CharField(max_length=100)),
                ('desc', models.CharField(max_length=100)),
                ('slug', models.CharField(max_length=100)),
                ('depa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='act2.Depa')),
            ],
        ),
    ]
