from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Depa, Soft
from .forms import DepaForm,SoftForm

#list
class List(generic.ListView):
	template_name = "act2/listsoft.html"
	queryset = Soft.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Soft.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre_soft__icontains=query))
		return qs

#create
class CreateSoft(generic.CreateView):
	template_name = "act2/createsoft.html"
	model = Soft
	form_class = SoftForm
	success_url = reverse_lazy("listsoft")

class CreateDepa(generic.CreateView):
	template_name = "act2/createdepa.html"
	model = Depa
	form_class = DepaForm
	success_url = reverse_lazy("listsoft")

class UpdateSoft(generic.UpdateView):
	template_name = "act2/updatesoft.html"
	model = Soft
	fields = [
    "name_soft",
    "desc",
    "depa",
    "slug",
    ]
	success_url = reverse_lazy("listsoft")

class UpdateDepa(generic.UpdateView):
	template_name = "act2/updatedepa.html"
	model = Depa
	fields = [
    "name_depa",
    "slug",
    ]
	success_url = reverse_lazy("listsoft")

class DetailSoft(generic.DetailView):
    template_name = "act2/detailsoft.html"
    model = Soft


class DeleteSoft(generic.DeleteView):
	template_name = "act2/deletesoft.html"
	model = Soft
	success_url = reverse_lazy("listsoft")

class DeleteDepa(generic.DeleteView):
	template_name = "act2/deletedepa.html"
	model = Depa
	success_url = reverse_lazy("listsoft")
