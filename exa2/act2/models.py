from django.db import models
from django.conf import settings
# Create your models here.

class Depa(models.Model):
    name_depa = models.CharField(max_length = 100)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.name_depa

class Soft(models.Model):
    name_soft = models.CharField(max_length = 100)
    desc = models.CharField(max_length = 100)
    depa = models.ForeignKey(Depa, on_delete = models.CASCADE)
    slug = models.CharField(max_length = 100)

    def __str__(self):
        return self.name_soft
