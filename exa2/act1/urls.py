from django.urls import path

from act1 import views

urlpatterns = [
	path('list_inv/', views.ListInv.as_view(), name="list_inv"),
    path('list_inv/1', views.ListInv1.as_view(), name="list_inv1"),
    path('list_inv/2', views.ListInv2.as_view(), name="list_inv2"),
    path('list_inv/3', views.ListInv3.as_view(), name="list_inv3"),
    path('list_inv/4', views.ListInv4.as_view(), name="list_inv4"),
    path('list_inv/5', views.ListInv5.as_view(), name="list_inv5"),
	path('create_inv/', views.CreateInv.as_view(), name="create_inv"),
	path('detail_inv/<int:pk>/', views.DetailInv.as_view(), name="detail_inv"),
	path('delete_inv/<int:pk>/', views.DeleteInv.as_view(), name="delete_inv"),
	path('update_inv/<int:pk>/', views.UpdateInv.as_view(), name="update_inv"),
]
