from django.db import models
#modelos
from django.conf import settings

class TraInv(models.Model):
    nombreinv = models.CharField(max_length=100)
    autores = models.CharField(max_length=100)
    categoria = models.CharField(max_length=100)
    archivo = models.FileField(null=True, blank=True)
    fecha = models.DateField(auto_now_add=True)
    slug = models.SlugField()

    def __str__(self):
        return self.nombreinv
