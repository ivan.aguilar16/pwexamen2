from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import TraInv
from .forms import TraInvForm

#Aqui las vistas
#Create
class CreateInv(generic.CreateView):
	template_name = "act1/create_inv.html"
	model = TraInv
	form_class = TraInvForm
	success_url = reverse_lazy("list_inv")

#RETRIEVE

class ListInv(generic.ListView):
    template_name = "act1/list_inv.html"
    model = TraInv
class ListInv1(generic.ListView):
    template_name = "act1/list_inv.html"
    queryset = TraInv.objects.filter(categoria="Ciencias Computacionales")
class ListInv2(generic.ListView):
    template_name = "act1/list_inv.html"
    queryset = TraInv.objects.filter(categoria="Ciencias de la tierra")
class ListInv3(generic.ListView):
    template_name = "act1/list_inv.html"
    queryset = TraInv.objects.filter(categoria="Ciencias Naturales")
class ListInv4(generic.ListView):
    template_name = "act1/list_inv.html"
    queryset = TraInv.objects.filter(categoria="Ciencias Sociales")
class ListInv5(generic.ListView):
    template_name = "act1/list_inv.html"
    queryset = TraInv.objects.filter(categoria="Ciencias Medicas")

class DetailInv(generic.DetailView):
    template_name = "act1/detail_inv.html"
    model = TraInv

#UPDATE

class UpdateInv(generic.UpdateView):
	template_name = "act1/update_inv.html"
	model = TraInv
	fields = [
        "nombreinv",
        "categoria",
        "autores",
        "slug",
    ]
	success_url = reverse_lazy("list_inv")

#Delete

class DeleteInv(generic.DeleteView):
	template_name = "act1/delete_inv.html"
	model = TraInv
	success_url = reverse_lazy("list_inv")
