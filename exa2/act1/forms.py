from django import forms

from .models import TraInv

class TraInvForm(forms.ModelForm):
    class Meta:
        model = TraInv
        fields = [
        "nombreinv",
        "categoria",
        "autores",
        "archivo",
        "slug",
        ]
